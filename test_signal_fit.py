
import utils

retriever = utils.get_v24_retriever()

mass_points = ["123", "124", "125", "126", "127"]
files = []
for mp in mass_points:
    files += retriever.get_root_files("ggH{}_".format(mp))

print(files)

frame = retriever.get_dataframe(files, ["m4l_unconstrained", "mass_point"])

import workspace_manager
import signal_model

#workspace_manager.get_variable("m4l_unconstrained", 120.0, 110.0, 160.0)
raw_params = signal_model.get_raw_params("m4l_unconstrained", "Inclusive", "NoBDT", "Nominal")
params = signal_model.get_params("m4l_unconstrained", "Inclusive", "NoBDT", "Nominal")

model = signal_model.load_signal_model(**params)

mp = workspace_manager.get_variable("mass_point")
fit_range = workspace_manager.get_global_fit_ranges()["m4l_unconstrained"] #this is from 105.0, 160.0

dataset = workspace_manager.get_dataset_from_df(frame,                                                ["m4l_unconstrained",                                                 "mass_point"],                                                selection="(event_type == 0) and (m4l_unconstrained > {low}) and (m4l_unconstrained < {high})".format(low=fit_range[0], high=fit_range[1]),                                                normalize_mass_points=False)

import ROOT
model.fitTo(dataset, ROOT.RooFit.ConditionalObservables(ROOT.RooArgSet(mp)), ROOT.RooFit.PrintLevel(3))

