# coding: utf-8

import ROOT
x = ROOT.RooRealVar("x", "x", -0.5, 0.5)
pt = ROOT.RooRealVar("pt", "pt", 20.0, 100.0)
mean = ROOT.RooRealVar("mu", "mu", 0.0)
r1_val = 0.015
r0_val = 0.020
r0 = ROOT.RooRealVar("r0", "r0", r0_val, 0.0, 5.0 * r0_val)
r1 = ROOT.RooRealVar("r1", "r1", r1_val, 0.0, 5.0 * r1_val)
sigma = ROOT.RooFormulaVar("formula", "formula", "(r0 + (r1 * pt))", ROOT.RooArgList(r1, r0, pt))
gaus = ROOT.RooGaussian("guas_ptconditional", "gaus_ptconditional", x, mean, sigma)
pt_dist = ROOT.RooBreitWigner ("breit", "breit", pt, ROOT.RooFit.RooConst(50.0), ROOT.RooFit.RooConst(15.0))

#join_pdf = ROOT.RooProdPdf("prod", "prod", pt_dist, ROOT.RooFit.Conditional(gaus, ROOT.RooArgSet(*[

proto_ptset = pt_dist.generate(ROOT.RooArgSet(pt), 10e5 * 2)
dataset = gaus.generate(ROOT.RooArgSet(x),  ROOT.RooFit.ProtoData(proto_ptset))
dataset.Print()

r1.setVal(2.0 * r1_val)
r0.setVal(0.3 * r0_val)

gaus.fitTo(dataset, ROOT.RooFit.ConditionalObservables(ROOT.RooArgSet(pt)), ROOT.RooFit.Optimize(False))


print("")
print("Injected Values")
print("Injected r1: {}".format(r1_val))
print("Injected r0: {}".format(r0_val))
print("")
print("Fitted Results:")
print("Fitted r1: {:.4f} +- {:.4f}".format(r1.getVal(), r1.getError()))
print("Fitted r0: {:.4f} +- {:.4f}".format(r0.getVal(), r0.getError()))
