import os

hmumu_files_dir = "/project/def-psavard/hmumu_harish_files/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/common_ntuples/v23/"
hzz_files_dir = "/project/def-psavard/MASSSCRATCHDIR/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/AntiKt4EMPFlow/__CAMPAIGN__/Nominal/"

zjets_dsids = [str(el) for el in range(364100, 364114)] + [str(el) for el in range(366300, 366309)] + ["308093"]
diboson_dsids = ["364250", "364253", "364254"]
ttbar_dsids = ["410472"]
signal_dsids = ["345097", "345106"]
campaigns = ["mc16a", "mc16d", "mc16e"]

data_files_dict = {}
data_files_dict["mc16a"] = ["data15.allYear.sideband.root", "data16.allYear.sideband.root"]
data_files_dict["mc16d"] = ["data17.allYear.sideband.root"]
data_files_dict["mc16e"] = ["data18.allYear.sideband.root"]

for key in data_files_dict:
    data_files_dict[key] = [os.path.join(hmumu_files_dir,el) for el in data_files_dict[key]]

zjets_files_dict = {}
diboson_files_dict = {}
ttbar_files_dict = {}
signal_files_dict = {}
for c in campaigns:
    zjets_files_dict[c] = []
    diboson_files_dict[c] = []
    ttbar_files_dict[c] = []
    signal_files_dict[c] = []
    for d in zjets_dsids:
        zjets_files_dict[c].append(os.path.join(hmumu_files_dir, ".".join([c, d, "root"])))
    for d in ttbar_dsids:
        ttbar_files_dict[c].append(os.path.join(hmumu_files_dir, ".".join([c, d, "root"])))
    for d in diboson_dsids:
        diboson_files_dict[c].append(os.path.join(hmumu_files_dir, ".".join([c,d,"root"])))
    for d in signal_dsids:
        signal_files_dict[c].append(os.path.join(hmumu_files_dir, ".".join([c,d,"root"])))

zjets_all_files = []
signal_all_files = []
ttbar_all_files = []
diboson_all_files = []
data_all_files = []
for key in zjets_files_dict:
    zjets_all_files += zjets_files_dict[key]
    signal_all_files += signal_files_dict[key]
    ttbar_all_files += ttbar_files_dict[key]
    data_all_files += data_files_dict[key]
    diboson_all_files += diboson_files_dict[key]

baseline_selection = "(PassesDiMuonSelection == 1) and (Muons_PT_Sub>15)"
baseline_weight = "GlobalWeight * SampleOverlapWeight * EventWeight_MCCleaning_5"
baseline_variables = ["PassesDiMuonSelection", "Muons_PT_Sub", "GlobalWeight", "SampleOverlapWeight", "EventWeight_MCCleaning_5"]
